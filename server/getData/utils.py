import psycopg2


class Utils:

    def __init__(self):
        self.connection = psycopg2.connect(user="alex", password="xxx", host="localhost", port="5432", database="landing-managment")
        self.cursor = self.connection.cursor()

    def __del__(self):
        self.cursor.close()
        self.connection.close()

    def create_tables(self):
        tables_f = open("sql_querry", "r")
        self.cursor.execute(tables_f.read())
        self.connection.commit()
        tables_f.close()

    def get_all_tables(self):
        self.cursor.execute("select table_name from information_schema.tables where table_schema = 'public'")
        tables = list()
        for row in self.cursor.fetchall():
            tables.append(row[0])
        self.connection.commit()
        return tables

    def get_columns_from_table(self, table):
        try:
            self.cursor.execute("select column_name from information_schema.columns "
                                "where table_name = '" + table + "' " +
                                "and column_name <> 'id'")
            res = list()
            for row in self.cursor.fetchall():
                res.append(row[0])
            return res
        except psycopg2.Error:
            pass

    def get_columns_type(self, table):
        try:
            self.cursor.execute("select data_type from information_schema.columns "
                                "where table_name = '" + table + "' " +
                                "and column_name <> 'id'")
            res = list()
            for row in self.cursor.fetchall():
                res.append(row[0])
            return res
        except psycopg2.Error:
            pass

    def get_columns_data(self, table, column):
        try:
            self.cursor.execute("select " + column + " from " + table)
            res = list()
            for row in self.cursor.fetchall():
                res.append(row[0])
            return res
        except psycopg2.Error:
            pass

    def drop_all_tables(self):
        for table in self.get_all_tables():
            self.cursor.execute("drop table " + table + " cascade")
        self.connection.commit()

    def insert(self):
        insert_f = open("insert.txt", "r")
        self.cursor.execute(insert_f.read())
        self.connection.commit()
        insert_f.close()

    def show_all(self):
        self.cursor.execute("select table_name from information_schema.tables where table_schema = 'public'")
        names = list()
        info = list()
        for row in self.cursor.fetchall():
            names.append(row[0])
            self.cursor.execute("select * from " + row[0])
            info.append(self.cursor.fetchall())
        for tab, name in zip(info, names):
            print("\n" + name)
            string = "\t"
            for col in self.get_columns_from_table(name):
                string += str(col) + "\t\t"
            print(string + "\n")
            for row in tab:
                string = "\t"
                for col in row:
                    string += str(col) + "\t\t"
                print(string)

    def select_from_table(self, table, selector):
        try:
            self.cursor.execute("select " + selector + " from " + table)
            res = self.cursor.fetchall()
            return res
        except psycopg2.Error:
            pass

    def input_into_table(self, table, columns, values):
        try:
            print("insert into " + table + " (" + columns + ") values (" + values + ")"
                  + " on conflict do nothing")
            self.cursor.execute("insert into " + table + " (" + columns + ") values (" + values + ")"
                                + " on conflict do nothing")

            self.connection.commit()
        except psycopg2.Error:
            pass

    def update_table(self, table, values, cond):
        try:
            self.cursor.execute("update " + table + " set " + values + " where " + cond)
            self.connection.commit()
        except psycopg2.Error:
            pass

    def delete_from_table(self, table, cond):
        try:
            self.cursor.execute("delete from " + table + " where " + cond)
            self.connection.commit()
        except psycopg2.Error:
            pass

from getData import utils


class Menu:

    def __init__(self, name, occupation, phone_number, car_model, model_year):
        self.utl = utils.Utils()
        self.name = str(name)
        self.occupation = str(occupation)
        self.phone_number = str(phone_number)
        self.car_model = str(car_model)
        self.model_year = str(model_year)
        self.insert()
        self.show_all()

    def show_all(self):
        self.utl.show_all()

    def insert(self):
        table = 'drivers'
        columns = 'name, occupation, phone_number, car_model, model_year'
        values = '\'' + self.name + '\', \'' + self.occupation + '\', \'' + self.phone_number + \
                 '\', \'' + self.car_model + '\', \'' + self.model_year + '\''
        self.utl.input_into_table(table, columns, values)


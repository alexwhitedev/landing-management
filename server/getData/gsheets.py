from __future__ import print_function
import pickle
import os.path
from googleapiclient.discovery import build
from google_auth_oauthlib.flow import InstalledAppFlow
from google.auth.transport.requests import Request


def gsheets(name, occupation, phone_number, car_model, model_year):
    # SETTINGS____
    SCOPES = ['https://www.googleapis.com/auth/spreadsheets']

    spreadsheet_id = '1epdzjbwQ6Bi0tvNzDbTeFvv9SdXEYB3ujBfW8edGemU'
    range_name = 'input_landing'
    value_input_option = 'RAW'

    creds = None
    if os.path.exists('token.pickle'):
        with open('token.pickle', 'rb') as token:
            creds = pickle.load(token)
    if not creds or not creds.valid:
        if creds and creds.expired and creds.refresh_token:
            creds.refresh(Request())
        else:
            flow = InstalledAppFlow.from_client_secrets_file(
                'credentials.json', SCOPES)
            creds = flow.run_local_server(port=0)
        with open('token.pickle', 'wb') as token:
            pickle.dump(creds, token)
    service = build('sheets', 'v4', credentials=creds)
    sheet = service.spreadsheets()

    # CODE_____

    # _READ
    result = sheet.values().get(spreadsheetId=spreadsheet_id,
                                range=range_name).execute()
    values = result.get('values', [])

    # _PRINT
    values = [
        [
            name, occupation, phone_number, car_model, model_year
        ]
    ]

    body = {
        'values': values
    }

    result = service.spreadsheets().values().append(
        spreadsheetId=spreadsheet_id, range=range_name,
        valueInputOption=value_input_option, body=body).execute()

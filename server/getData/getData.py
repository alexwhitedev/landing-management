from getData import app
from getData import dbwrite
from flask import request, jsonify
from getData import utils
from getData import gsheets


@app.route("/newperson", methods=['POST'])
def getData():
    occupation = request.form['city']
    name = request.form['name']
    phone_number = request.form['number']
    car_model = request.form['model']
    model_year = request.form['year']

    dbwrite.Menu(name, occupation, phone_number, car_model, model_year)
    gsheets.gsheets(name, occupation, phone_number, car_model, model_year)



    return jsonify({"data": str(occupation + ' ' + name + ' ' + phone_number + ' ' + car_model + ' ' + model_year)})

from flask import Flask
from flask_cors import CORS


app = Flask(__name__)
CORS(app)


import getData.getData
import getData.dbwrite
import getData.utils



import logging
import telegram
from telegram.ext import Updater, CommandHandler, MessageHandler, Filters, CallbackQueryHandler
from sheets import gsheets, gsheets_write


updater = Updater(token='901931449:AAFSPsM4ytN7Saq_hSPQ0a1wZVlAc0WNLgI', use_context=True)
# bot = telegram.Bot(token='901931449:AAFSPsM4ytN7Saq_hSPQ0a1wZVlAc0WNLgI')

dispatcher = updater.dispatcher
job = updater.job_queue

logging.basicConfig(level=logging.INFO, format='%(asctime)s - %(name)s - %(levelname)s - %(message)s')


def str_to_dict(str):
    str = str.replace('\n', ' ').split(':')
    str = str[1:]
    ret = []
    for i in str:
        if i != 'Occupation' or i != 'Name' or i != 'Tel.' or i != 'Car' or i != 'Year':
            ret.append(i.replace(' ', ''))
    return str


def start(update, context):
    context.bot.send_message(chat_id=update.message.chat_id, text="I'm a bot that help u work with google sheets")


count = 80
keyboard = [[telegram.InlineKeyboardButton('Accept', callback_data='accepted')]]
reply_markup = telegram.InlineKeyboardMarkup(keyboard)


def gsheet_ckecking(context: telegram.ext.CallbackContext):
    global count
    if len(gsheets()) > count:
        tmp = gsheets()
        if len(tmp[count]) == 5:
            send_message = 'Новая заявка:\n' + 'Occupation: ' + tmp[count][0] + '\n' + \
                           'Name: ' + tmp[count][1] + '\n' + \
                           'Tel.: ' + tmp[count][2] + '\n' + \
                           'Car: ' + tmp[count][3] + '\n' + \
                           'Year: ' + tmp[count][4]
            context.bot.send_message(chat_id='-349624243', text=send_message, reply_markup=reply_markup)
            count += 1


def button(update, context):
    query = update.callback_query
    tmp = query.message.text
    lst = str_to_dict(tmp)
    gsheets_write(lst[0], lst[1], lst[2], lst[3], lst[4], str(query.from_user.username))
    query.edit_message_text(text='accepted by: ' + str(query.from_user.username))



start_handler = CommandHandler('start', start)
dispatcher.add_handler(start_handler)

dispatcher.add_handler(CallbackQueryHandler(button))


job.run_repeating(gsheet_ckecking, interval=5, first=0)


updater.start_polling()
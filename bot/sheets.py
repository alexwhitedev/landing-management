from __future__ import print_function
import pickle
import os.path
from googleapiclient.discovery import build
from google_auth_oauthlib.flow import InstalledAppFlow
from google.auth.transport.requests import Request


# SETTINGS____
SCOPES = ['https://www.googleapis.com/auth/spreadsheets']

spreadsheet_id = '1epdzjbwQ6Bi0tvNzDbTeFvv9SdXEYB3ujBfW8edGemU'
range_name_read = 'input_landing'
range_name_write = 'call_page'
value_input_option = 'RAW'


creds = None
if os.path.exists('token.pickle'):
    with open('token.pickle', 'rb') as token:
        creds = pickle.load(token)
if not creds or not creds.valid:
    if creds and creds.expired and creds.refresh_token:
        creds.refresh(Request())
    else:
        flow = InstalledAppFlow.from_client_secrets_file(
            'credentials.json', SCOPES)
        creds = flow.run_local_server(port=0)
    with open('token.pickle', 'wb') as token:
        pickle.dump(creds, token)
service = build('sheets', 'v4', credentials=creds)
sheet = service.spreadsheets()


# CODE_____
def gsheets():
    # _READ

    # Counter for sheet's not empty rows. If count more then previously then we have new cells.
    result = sheet.values().get(spreadsheetId=spreadsheet_id,
                                range=range_name_read).execute()
    values = result.get('values', [])
    
    return values

    # try to get sheet info(rowCount for this task), but it gets all rows (empty including)
    # result = sheet.get(spreadsheetId=spreadsheet_id, fields='sheets.properties').execute()
    # values = result


def gsheets_write(occupation, name, phone_number, car_model, model_year, user_name):
    values = [
        [
            name, occupation, phone_number, car_model, model_year, user_name
        ]
    ]

    body = {
        'values': values
    }

    result = service.spreadsheets().values().append(
        spreadsheetId=spreadsheet_id, range=range_name_write,
        valueInputOption=value_input_option, body=body).execute()